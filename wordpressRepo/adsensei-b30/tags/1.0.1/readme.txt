=== AdSensei b30===
Contributors: blogger30, marquesdecarabas
Donate link: http://blogger3cero.com/adsensei/donacion
Tags: adsense, ads, posts
Requires at least: 3.6+
Tested up to: 4.5
Stable tag: 1.0.1
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Añade fácil, rápido y flexible anuncios de AdSense por todo tu blog

== Description ==

**AdSensei B30** inserta tus anuncios de AdSense o de cualquier otro proveedor en cualquier parte del contenido de tus posts de la forma más flexible y precisa que existe

= Características =
* Extremadamente sencillo y fácil de configurar. Sin pestañas, en único scroll podrás ver todo
* Compatible con AdSense y cualquier otro proveedor de anuncios que funcione de manera similar
* Hasta 10 anuncios insertables en cualquier parte del contenido de los posts
* Flexible: la inserción de los anuncios es configurable: al comienzo del post, a mitad del post, al final, después del párrafo 1,2,3…10, después de la primera, segunda o tercera etiqueta h2 o h3... etc
* Rapidez: al nivel de cualquier otro plugin de AdSense que puedas encontrar
* Ligero: ocupa menos de 20Kbs, frente a cientos de Kbs de plugins similares


== Installation ==
1. Puedes:
 * Subir la carpeta `adsensei-b30` al directorio `/wp-content/plugins/` vía FTP. 
 * Subir el archivo ZIP completo vía *Plugins -> Añadir nuevo -> Subir* en el Panel de Administración de tu instalación de WordPress.
 * Buscar **AdSensei B30** en el buscador disponible en *Plugins -> Añadir nuevo* y pulsar el botón *Instalar ahora*.
2. Activar el plugin a través del menú *Plugins* en el Panel de Administración de WordPress.
3. Configurar el plugin con el botón *Ajustes* o en *Ajustes -> Enlaces permanentes*.
4. Listo, ahora ya puedes disfrutar de él, y si te gusta y te resulta útil, hacer una donación.

== Frequently asked questions ==
= ¿Cómo se configura? =

 * Pegar el código AdSense en cualquiera de los hasta 10 anuncios disponibles, indicar en que posición quieres que salga, elegir alineación y margen.
 * Se pueden configurar hasta 10 anuncios en total, cada uno en una posición diferente. 
 * Pulsar botón guardar y el proceso habrá terminado

== Screenshots ==
1. Captura de pantalla de **AdSensei B30**.

== Changelog ==
= 1.0.1 =
* Arreglados bugs de filtrado 'sólo en posts' y el fondo amarillo en los anuncios
= 1.0 =
* Primera versión liberada

== Gracias ==
* AdSensei B30 ha sido desarrollado por Jaime Sempere y Dean Romero con mucha ilusión para servir a cualquier blogger de habla hispana. Nos hemos concentrado en hacer un plugin muy sencillo pero flexible, sin perder potencia ni mucho menos funcionalidades y siendo ligero y fácil de usar. Esperamos que te guste. Puedes pedirnos más características y darnos sugerencias para futuras versiones en (http://blogger3cero.com/)

¡Muchas gracias a todos!
