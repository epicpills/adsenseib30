



function getAdNameByNumber(adNumber){
  return jQuery("#adsenseib30_settings\\[adName" + adNumber +"\\]");
}
function getAdTextAreaByNumber(adNumber){
  return jQuery("#adsenseib30_settings\\[adCode" + adNumber +"\\]");
}
function getAdEnableStateByNumber(adNumber){
  return jQuery("#adsenseib30_settings\\[adEnabled" + adNumber +"\\]");
}
function getAdTitle(adNumber){
  return jQuery(".rm_title.ad" + adNumber);
}
function getAdSection(adNumber){
  return jQuery(".rm_section.ad" + adNumber);
}

function changeTitleName(adNumber, newValue){
  var title = jQuery("#adTitle"+adNumber);
  title.html(newValue);
}

function changeNameOfAd(adNumber, newValue){

  var ad = getAdNameByNumber(adNumber);
  ad.val(newValue);

  changeTitleName(adNumber, newValue);
}


function newName(adNumber) {
    var title = prompt("Introduce un nuevo nombre para este anuncio", "");

    if (title != null) {
        changeNameOfAd(adNumber,title);
    }
}

function adEnableState(adNumber, enabled){
  var adTextArea = getAdTextAreaByNumber(adNumber);
  if (enabled) {
    adTextArea.attr("readonly","false");
  } else {
    adTextArea.attr("readonly","readonly");
  }
  var adEnableState = getAdEnableStateByNumber(adNumber);
  adEnableState.val(enabled);

  var title = getAdTitle(adNumber);
  title.toggleClass("titleDisabled")

  var section = getAdSection(adNumber);
  section.toggleClass("sectionDisabled")

}

function disableAd(adNumber){

  addEnableLink = jQuery("#enableAd"+adNumber);
  addDisableLink = jQuery("#disableAd"+adNumber);
  addEnableLink.show();
  addDisableLink.hide();
  adEnableState(adNumber, "false");

}
function enableAd(adNumber){
  addEnableLink = jQuery("#enableAd"+adNumber);
  addDisableLink = jQuery("#disableAd"+adNumber);
  addEnableLink.hide();
  addDisableLink.show();
  adEnableState(adNumber,"true");
}

