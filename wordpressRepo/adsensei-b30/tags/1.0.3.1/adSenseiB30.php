<?php

/*
  Plugin Name: AdSensei B30
  Plugin URI: http://adSenseiB30
  Description: Inserta de manera fácil, rápida y flexible anuncios de AdSense por todo tu blog
  Version: 1.0.3.1
  Author: Jaime Sempere, Dean Romero
  Author URI: http://blogger3cero.com/
  License: GPLv2 or later
 */

/******************************
* global variables
******************************/

class AdSensei_B30 {
	/** Singleton *************************************************************/
	/**
	 */
	private static $instance;

  public static function instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new AdSensei_B30;
			self::$instance->includes();
			self::$instance->init();
		}
		return self::$instance;
	}

	private function includes() {
		include_once( dirname( __FILE__ ) . '/includes/adsb30_admin_page.php' );
	}

	/** Filters & Actions **/
	private function init() {
    add_action('admin_init', 'adsenseib30_register_settings');
    add_action('admin_menu', array($this, 'admin_menu_function'));
    add_action('admin_enqueue_scripts', array($this, 'adsb30_load_scripts'));
    add_filter('the_content', array($this, 'core_method'));
    add_shortcode( 'sin_anuncios_b30', array($this,'do_nothing_shortcode'));
    add_shortcode( 'anuncio_b30', array($this,'show_add_shortcode'));
  }

  public function do_nothing_shortcode( $atts, $content = null ) {
      return $content;
  }

  public function show_add_shortcode( $atts, $content ) {
      $ad_shortcode = shortcode_atts(array(
        'id' => 1
      ), $atts);

      $numberOfAd = ($ad_shortcode['id']);
      $adsenseib30_settings = get_option('adsenseib30_settings');
      $myad = $adsenseib30_settings['adCode'.$numberOfAd];
      $myad = $this->wrap_ad_into_div($myad, $adsenseib30_settings, $numberOfAd);

      return $myad;
  }


  public function core_method($content) {

    if( has_shortcode( $content, 'sin_anuncios_b30' ) ) return $content;
    if( has_shortcode( $content, 'anuncio_b30' ) ) return $content;

    if (!(is_page() || (is_single()))) return $content;

    $content = preg_replace('/(<p.*id="more.*<\\/p>)/U', '', $content,1);

    $adsenseib30_settings = get_option('adsenseib30_settings');
    $content = $this->load_all_ads( $content, $adsenseib30_settings);


    return $content." <style> ins.adsbygoogle { background: transparent !important; } </style>";
  }

  public function load_all_ads($content, $adsenseib30_settings){

    for ($numberOfAd = 1; $numberOfAd <= 10; $numberOfAd++) {
      $content = $this->addAdAfterParagraphOrAfterH2H3($content, $adsenseib30_settings, $numberOfAd);
    }

    return $content;
  }

  private function addAdAfterParagraphOrAfterH2H3($content, $adsenseib30_settings, $numberOfAd){

    $showOn = $adsenseib30_settings['showOn'.$numberOfAd];
    if ($showOn == 'posts') if (is_page()) return $content;
    if ($showOn == 'pages') if (is_single()) return $content;

    $myad = $adsenseib30_settings['adCode'.$numberOfAd];
    if (strlen(trim($myad)) == 0) return $content;

    $adPosition = $adsenseib30_settings['adPosition'.$numberOfAd];
    $myad = $this->wrap_ad_into_div($myad, $adsenseib30_settings, $numberOfAd);

    if (strpos($adPosition, 'H') === false) {
      return $this->addAdAfterParagraph($content, $myad, $adPosition);
    }
    else {
      return $this->replaceH2H3($content, $myad, $adPosition);
    }
  }

  private function replaceH2H3($content, $myad, $adPosition){

    if (strpos($adPosition, 'first') !== false) $realAdPosition = 1;
    if (strpos($adPosition, 'second') !== false) $realAdPosition = 2;
    if (strpos($adPosition, 'third') !== false) $realAdPosition = 3;

    if (strpos($adPosition, 'H2') !== false) {
      return preg_replace('/(<h2.*<\\/h2>.*){'.$realAdPosition.'}/Us', '${0}'.$myad, $content,1);
    }
    if (strpos($adPosition, 'H3') !== false) {
      return preg_replace('/(<h3.*<\\/h3>.*){'.$realAdPosition.'}/Us', '${0}'.$myad, $content,1);
    }
    return $content;
  }

  private function addAdAfterParagraph($content, $myad, $adPosition){

-   $paragraphs = preg_match_all('/<p.*<\\/p>.*/isU', $content, $output_array);

    $realAdPosition = $this->get_real_ad_position($adPosition, ($paragraphs));
    return preg_replace('/(<p.*<\\/p>.*){'.$realAdPosition.'}/sU', '${0}'.$myad, $content,1);

    return $content;
  }

  private function replaceAdInH2H3($content, $adsenseib30_settings, $numberOfAd){

    $myad = $adsenseib30_settings['adCode'.$numberOfAd];
    if (strlen(trim($myad)) == 0) return $content;
    $adPosition = $adsenseib30_settings['adPosition'.$numberOfAd];

    if (strpos($adPosition, 'H') == false) return $content;

    $myad = $this->wrap_ad_into_div($myad, $adsenseib30_settings, $numberOfAd);

    if (strpos($adPosition, 'first') !== false) $realAdPosition = 1;
    if (strpos($adPosition, 'second') !== false) $realAdPosition = 2;
    if (strpos($adPosition, 'third') !== false) $realAdPosition = 3;

    if (strpos($adPosition, 'H2') !== false) {
      return preg_replace('/(<h2.*<\\/h2>.*){'.$realAdPosition.'}/Us', '${0}'.$myad, $content,1);
    }
    if (strpos($adPosition, 'H3') !== false) {
      return preg_replace('/(<h3.*<\\/h3>.*){'.$realAdPosition.'}/Us', '${0}'.$myad, $content,1);
    }
    return $content;
  }


  private function wrap_ad_into_div($myad, $adsenseib30_settings, $numberOfAd){
    $margin = $adsenseib30_settings['adMargin'.$numberOfAd];
    $align = $adsenseib30_settings['adAlign'.$numberOfAd];
    if (($align == 'wrapleft') ){
        return '<div class="adsb30" style="margin:'.$margin.'px; margin-left:0px; float:left">'.$myad.'</div>';
    }
    if (($align == 'left')){
        return '<div class="adsb30" style="margin:'.$margin.'px; margin-left:0px; text-align:left">'.$myad.'</div>';
    }
    if (($align == 'wrapright')){
        return '<div class="adsb30" style="margin:'.$margin.'px; margin-right:0px; float:right">'.$myad.'</div>';
    }
    if (($align == 'right')){
        return '<div class="adsb30" style="margin:'.$margin.'px; margin-right:0px; text-align:right">'.$myad.'</div>';
    }
    return '<div class="adsb30" style="margin:'.$margin.'px; text-align:'.$align.'">'.$myad.'</div>';
  }

  private function get_real_ad_position($ad1Position, $paragrahpsNum){
    if ($ad1Position == 'beginning') return 0;
    if ($ad1Position == 'middle') return ((floor($paragrahpsNum/2)));
    if ($ad1Position == 'end') return $paragrahpsNum;
    if ($ad1Position == 'before end') return ($paragrahpsNum - 1);
    if ($ad1Position > $paragrahpsNum) return $paragrahpsNum;
    return $ad1Position;
  }

  public function adsb30_load_scripts() {
    wp_enqueue_style('adsb30-admin-styles', plugin_dir_url( __FILE__ ) . 'includes/css/adsB30AdminPanel.css');
  }

  public function admin_menu_function(){
    add_menu_page ( 'AdSensei B30 Admin', 'AdSensei B30',
                    'edit_pages', 'adsensei-admin',
                    'adsenseib30_settings_page');
  }

}


/** SINGLETON INVOCATION **/
function adSensei_load() {
	return AdSensei_B30::instance();
}

adSensei_load();

